#loading packages
import pandas as pd
import numpy as np
import pyodbc
import sqlalchemy
import urllib
import cx_Oracle
import os
import re
import shutil
import datetime
from pandas import DataFrame

# need to load in all of the files in the directory
# read in using the map functions
os.chdir('//ntserver/pa/PA_DB/SN_test_ETL')
file_list = os.listdir()
file_list = pd.DataFrame(file_list)
file_list['file_type'] = ""
for file in range(len(file_list)):
    file_name = file_list.loc[file, 0]
    file_name_value = (file_name.split("_")[0])
    file_list.loc[file, 'file_type'] = file_name_value
print(file_list)

incoming_size_header = pd.DataFrame()
filter_file_list = file_list[file_list.file_type == 'sizehdr'][0]

for file in (filter_file_list):
    temp_file = pd.read_csv(file, sep="|")
    incoming_size_header = incoming_size_header.append(temp_file)

incoming_size_header.created_year = incoming_size_header.created_year.astype(str)
incoming_size_header['New_size_scale'] = 0
incoming_size_header['Cat_value'] = (incoming_size_header.scale_name + "_" + (incoming_size_header.created_year) + "_" + incoming_size_header.created_season)
   
filter_file_list = file_list[file_list.file_type == 'sizescales'][0]
incoming_size_sizescales = pd.DataFrame()

for file in (filter_file_list):
    temp_file = pd.read_csv(file, sep="|")
    incoming_size_sizescales = incoming_size_sizescales.append(temp_file)


filter_file_list = file_list[file_list.file_type == 'sizeprof'][0]
incoming_size_sizeprof = pd.DataFrame()

for file in (filter_file_list):
    temp_file = pd.read_csv(file, sep="|")
    incoming_size_sizeprof = incoming_size_sizeprof.append(temp_file)
    
# need to load the data from sql server data
# just need to pull in the header table
#NEED TO MAKE THIS READ FROM SF
conn_str = (
    r'Driver=SQL Server;'
    r'Server=CASQL01PC2\APP02;'
    r'Database=PAModelData;'
    r'Trusted_Connection=yes;')

cnxn = pyodbc.connect(r'Driver={SQL Server};Server=CASQL01PC2\APP02;Database=PAModelData;Trusted_Connection=yes;')
cursor = cnxn.cursor()
max_data = cursor.execute("""SELECT  (SELECT MAX(size_scale_id) as Max_size_scale_id FROM  PAModelData.dbo.size_scale_header) as Max_size_scale_id,
		                              (SELECT max(size_profile_detail_id) FROM PAModelData.dbo.size_scale_detail ) as size_profile_detail_id_DID,
		                              (SELECT MAX(size_profile_detail_id) FROM PAModelData.dbo.size_scale_profile) as size_profile_detail_id_SP""")                                     
         
max_data_DF = pd.DataFrame([tuple(t) for t in max_data],
                  columns=('max_size_scale_id','size_profile_detail_id_DID','size_profile_detail_id_SP'))

#Making the counting varables
max_size_ID = pd.array(max_data_DF.max_size_scale_id)
max_size_profile_DID = pd.array(max_data_DF.size_profile_detail_id_DID)

size_scale_ID = cursor.execute("""SELECT DISTINCT size_scale_id, scale_name FROM PAModelData.dbo.size_scale_header""")                                      

size_scale_ID_DF = pd.DataFrame([tuple(t)for t in size_scale_ID],
                                    columns= ("size_scale_id","scale_name"))

header_table = cursor.execute("""SELECT size_scale_id,active_size_scale,season_year,season,
                                    scale_name,department,class,size_count,
                                    size_range,size_list,profile_count,gender,creator,body_style,genre,license,notes,
                                    CONCAT(scale_name,'_',season_year,'_',season) as cat_value,updated_date
                                    FROM PAModelData.dbo.size_scale_header""")

header_table_DF = pd.DataFrame([tuple(t) for t in header_table],
                                  columns = ("size_scale_id","active_size_scale","season_year","season","scale_name","department","class",
                                             "size_count","size_range","size_list","profile_count","gender","creator","body_style","genre",
                                             "license","notes","cat_value","updated_date"))

#For Size Scales that already have a size_scales_ID
#getting the newest Value for size scale 
AE_header = pd.DataFrame(incoming_size_header[pd.notnull(incoming_size_header['scale_id'])].groupby(["scale_name","temp_id","Cat_value"])["created_dttm"].max())
AE_header.reset_index(inplace=True)

incoming_size_header = incoming_size_header[incoming_size_header['temp_id'].isin(AE_header['temp_id']) |  pd.isnull(incoming_size_header['scale_id']) ]
                       
    
#For size scales that are new and dont have a scale ID.
#Adding size scales, updating size_profile detail ID.
new_sch = pd.DataFrame(incoming_size_header[pd.isnull(incoming_size_header['scale_id'])].groupby(["scale_name","temp_id"])["created_dttm"].max())
new_sch["scale_id_value"] = 0
new_sch.reset_index(inplace=True)
for size_name in range(len(new_sch)):
    new_sch.iloc[size_name,3] =  (max_size_ID + 1)
    max_size_ID = max_size_ID +1 
    
for size_name in range(len(incoming_size_header)):
    if(pd.isnull(incoming_size_header.iloc[size_name,1])):
        print(size_name)
        scale_id = pd.array( new_sch[new_sch.temp_id == incoming_size_header.iloc[size_name,0]].scale_id_value)
        temp_id_value = incoming_size_header.iloc[size_name,0]
        size_profile_DID = max_size_profile_DID + 1
        incoming_size_header.iloc[size_name,1] = scale_id
        incoming_size_header.iloc[size_name,21] = 1
        incoming_size_header.iloc[size_name,2] = size_profile_DID
        #updating the size scale prof and size_sizeScales
        
        incoming_size_sizeprof.loc[incoming_size_sizeprof['temp_id'] == temp_id_value, 'scale_id'] = scale_id[0]
        incoming_size_sizeprof.loc[incoming_size_sizeprof['temp_id'] == temp_id_value, 'size_profile_detail_id'] = size_profile_DID[0]    
        incoming_size_sizescales.loc[incoming_size_sizescales['temp_id'] == temp_id_value, 'scale_id'] = scale_id[0]
        incoming_size_sizescales.loc[incoming_size_sizescales['temp_id'] == temp_id_value,'size_profile_detail_id'] = size_profile_DID[0]
        
        #updating the max_size_profile_DID value
        max_size_profile_DID = max_size_profile_DID + 1
        

loading_header_data = incoming_size_header 
loading_size_profile = incoming_size_sizeprof[incoming_size_sizeprof['temp_id'].isin(incoming_size_header['temp_id'])]
loading_size_scales = incoming_size_sizescales[incoming_size_sizescales['temp_id'].isin(incoming_size_header['temp_id'])]


#Need to change this to load into SF Data Base.
loading_header_data.to_csv("head_data")
loading_size_profile.to_csv("size_profile")
loading_size_scales.to_csv("size_sclaes")

